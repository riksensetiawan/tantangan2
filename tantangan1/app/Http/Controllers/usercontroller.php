<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\karyawan;

class usercontroller extends Controller
{

    public function index()
    {
        $data = karyawan::all();
        return view('welcome', compact('data'));
    }

    public function viewdelete()
    {
        $data = karyawan::all();
        return view('deletedata',compact('data'));
    }

    public function viewedit()
    {
        $data = karyawan::all();
        return view('editview', compact('data'));
    }

    public function viewadd()
    {
        return view('adddata');
    }

    public function store(Request $request)
    {
        karyawan::create([
            'name' => $request -> insert_name ,
            'email' => $request -> insert_email   
        ]);

        return back();       
    }

    public function deletedata($id)
    {   
         karyawan::destroy($id);
         return back();
    }

    public function editdata($id)
    {
        $data = karyawan::findorfail($id);
        return view('editdataview', compact('data'));
    }

    public function updatedata(Request $request,$id)
    {
        karyawan::findorfail($id)->update([
        'name' => $request -> insert_name ,
        'email' => $request -> insert_email 
        ]);
        return back();
    }

}
