<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>




<body>
    <div class="navcontainer">
    <a href="{{route('home')}}">Home</a>
    <a href="{{route('store_view')}}">Add New</a>
    <a href="{{route('edit_view')}}">Edit Existing</a>
    <a href="\">Delete</a>
</div>

<div class="datacontainers">
    <div class="datacontainer">
        <span>ID</span>
        <span >Nama</span>
        <span >Email</span>
    </div>
    <br>
@foreach($data as $datas)
    <div class="datacontainer">
        <span>{{$datas->id}}</span>  
        <span>{{$datas->name}}</span>
        <span>{{$datas->email}}</span>

        <form action="{{route('delete_data', $datas->id)}}" method="POST">
            @csrf
            <button type="submit">Delete</button>
            @method('DELETE')
        </form>
     </div>
 @endforeach
</div>

    
</body>
</html>