<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'usercontroller@index')->name('home');

Route::get('/add', 'usercontroller@viewadd')->name('store_view');

Route::post('/add','usercontroller@store')->name('storedata');

Route::get('/delete','usercontroller@viewdelete')->name('delete_view');

Route::delete('/delete/{id}', 'usercontroller@deletedata')->name('delete_data');

Route::get('/edit', 'usercontroller@viewedit')->name('edit_view');

Route::get('/editdata/{id}', 'usercontroller@editdata')->name('edit_data');

Route::patch('/update/{id}', 'usercontroller@updatedata')->name('update_data');